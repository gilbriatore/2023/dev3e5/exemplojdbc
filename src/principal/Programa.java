package principal;

import java.sql.*;

public class Programa {
	
	public static void main(String[] args) {
				
		Pessoa p = new Pessoa(11, "Joslei dos Santos", "877687", 2.1);				
		//salvarPessoa(p);				
		//atualizarPessoa(p);
		excluirPessoa(6);  
		listarPessoas();
		
	}
	
    public static void excluirPessoa(int id) {
		
		try {
			
			//1. Abrir a conexão
			String url = "jdbc:mysql://localhost:3306/exemplojdbc";
			String login = "root";
			String senha = "positivo";
			
			String sql = "DELETE FROM pessoas WHERE id = ?;";
			
			Connection con = DriverManager.getConnection(url, login, senha);
			
			//2. Criar o comando e executar a consulta
			PreparedStatement comando = con.prepareStatement(sql);
	
			comando.setInt(1, id);
			
			comando.executeUpdate();

			comando.close();
			con.close();
	  } catch(SQLException e) {
		  System.out.println("Erro ao excluir pessoa!");
	  }
		
	}
	
	public static void atualizarPessoa(Pessoa pessoa) {
		
		try {
			
			//1. Abrir a conexão
			String url = "jdbc:mysql://localhost:3306/exemplojdbc";
			String login = "root";
			String senha = "positivo";
			
			String sql = "UPDATE pessoas SET `nome` = ?, `cpf` = ?, `altura` = ? WHERE `id` = ?;";
			
			Connection con = DriverManager.getConnection(url, login, senha);
			
			//2. Criar o comando e executar a consulta
			PreparedStatement comando = con.prepareStatement(sql);
			comando.setString(1, pessoa.getNome());
			comando.setString(2, pessoa.getCpf());
			comando.setDouble(3, pessoa.getAltura());
			
			comando.setInt(4, pessoa.getId());
			
			comando.executeUpdate();

			comando.close();
			con.close();
	  } catch(SQLException e) {
		  System.out.println("Erro ao atualizar pessoa!");
	  }
		
	}
	
	public static void salvarPessoa(Pessoa pessoa) {
		
		try {
			
			//1. Abrir a conexão
			String url = "jdbc:mysql://localhost:3306/exemplojdbc";
			String login = "root";
			String senha = "positivo";
			
			String sql = "INSERT INTO pessoas (`nome`,`cpf`,`altura`) VALUES (?,?,?);";
			
			Connection con = DriverManager.getConnection(url, login, senha);
			
			//2. Criar o comando e executar a consulta
			PreparedStatement comando = con.prepareStatement(sql);
			comando.setString(1, pessoa.getNome());
			comando.setString(2, pessoa.getCpf());
			comando.setDouble(3, pessoa.getAltura());
			
			comando.executeUpdate();

			comando.close();
			con.close();
	  } catch(SQLException e) {
		  System.out.println("Erro ao salvar pessoa!");
	  }
		
	}
	
	
	public static void listarPessoas()  {
		
		try {
			
			//1. Abrir a conexão
			String url = "jdbc:mysql://localhost:3306/exemplojdbc";
			String login = "root";
			String senha = "positivo";
			
			String sql = "SELECT * FROM pessoas;";
			
			Connection con = DriverManager.getConnection(url, login, senha);
			
			//2. Criar o comando e executar a consulta
			Statement comando = con.createStatement();
			ResultSet resultado = comando.executeQuery(sql);
			
			//3. Utilizar os dados
			while(resultado.next()) {
				int id = resultado.getInt("id");
				String nome = resultado.getString("nome");
				String cpf = resultado.getString("cpf");
				double altura = resultado.getDouble("altura");
				
				System.out.println("----------------");
				System.out.println("Id: " + id);
				System.out.println("Nome: " + nome);
				System.out.println("CPF: " + cpf);
				System.out.println("Altura: " + altura); 
			}
			comando.close();
			con.close();
	  } catch(SQLException e) {
		  System.out.println("Erro na execução da listagem!");
	  }
		
	}

}
